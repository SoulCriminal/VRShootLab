using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.GraphicsBuffer;

public class TargetManager : MonoBehaviour
{
    public Transform topLeftCorner;
    public Transform topRightCorner;
    public Transform bottomLeftCorner;
    public Transform bottomRightCorner;

    public GameObject targetPrefab;
    public int initialTargetsCount = 3;
    public int score = 0;

    public TextMeshProUGUI countdownText;
    public TextMeshProUGUI scoreText;
    [SerializeField] Button startButton;
    private float countdownTime = 60f;
    public bool isCountdownActive = false;

    [SerializeField]  Animator doorAnim;
    [SerializeField] AudioSource doorOpenSound;

    public static TargetManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        
    }

    void Start()
    {
        //StartCountdown(); // Oyun ba�lad���nda countdown'u ba�lat.
    }

    void Update()
    {
        if (isCountdownActive)
        {
            doorAnim.SetBool("Open", true);
            // Countdown i�lemleri
            countdownTime -= Time.deltaTime;
            countdownText.text = "Time: " + Mathf.Round(countdownTime);
            scoreText.text = score.ToString();
            if (countdownTime <= 0f)
            {
                EndGame();
            }
        }
        if(score > 3000)
        {
            doorAnim.SetBool("Open", true);
            doorOpenSound.Play();
        }
    }

    public void StartCountdown()
    {
        score = 0; // Oyun her başladığında score 0 olmalı
        isCountdownActive = true;
        countdownTime = 60f;
        countdownText.text = "Left Time: " + countdownTime;
        VRController.isStart = true; // Ateş edebilmek için
        SpawnInitialTargets(); // Countdown başladığında hedefleri spawnla.
    }

    void SpawnInitialTargets()
    {
        for (int i = 0; i < initialTargetsCount; i++)
        {
            SpawnRandomTarget();
        }
    }

    public void SpawnRandomTarget()
    {
        float randomX = Random.Range(topLeftCorner.position.x, topRightCorner.position.x);
        float randomY = Random.Range(topLeftCorner.position.y, bottomLeftCorner.position.y);
        float zPosition = topLeftCorner.position.z;

        Vector3 spawnPosition = new Vector3(randomX, randomY, zPosition);

        GameObject newTarget = Instantiate(targetPrefab, spawnPosition, Quaternion.identity);
   
    }

 

    public void IncreaseScore()
    {
        Debug.Log(scoreText.text);
        score += 10;
    }

    void EndGame()
    {
        isCountdownActive = false;

        // Oyun bittiğinde yapılacak işlemler 
        DestroyAllTargets();
        //Start Button Aktif etmek için
        startButton.enabled = true;
        //Ateş etme event sonlandırmak için
        VRController.isStart =false;
    }

    void DestroyAllTargets()
    {
        GameObject[] targets = GameObject.FindGameObjectsWithTag("Enemy"); // Hedef objeleri bul

        foreach (GameObject target in targets)
        {
            Destroy(target); // Hedef objelerini yok et
        }
    }
}
