using TMPro;
using UnityEngine;

public class ShootingStats : MonoBehaviour
{
    public static int totalShots = 0;
    private int successfulHits = 0;
    private float accuracyPercentage = 100.0f;

    public static ShootingStats Instance; // Singleton instance
    [SerializeField] TextMeshProUGUI hitPercentage;

    private void Awake()
    {
        // Singleton pattern
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        UpdateAccuracy(); // Ba�lang��ta do�ru ba�ar� oran�n� g�stermek i�in
    }
    private void Update()
    {
        Debug.Log(totalShots);
    }

    // Ate� edildi�inde bu fonksiyon �a�r�l�r.
    public void FireStats(bool isHit)
    {

        // E�er hedef vurulduysa, ba�ar� say�s�n� artt�r.
        if (isHit)
        {
            successfulHits++;
        }

        // Her at�� sonras�nda ba�ar� oran�n� g�ncelle
        UpdateAccuracy();

        // �statistikleri yazd�r (istedi�iniz yerde kullan�labilir).
        Debug.Log($"Total Shots: {totalShots}, Successful Hits: {successfulHits}, Accuracy: {accuracyPercentage}%");
    }

    // Genel isabet oran�n� g�ncelle
    public void UpdateAccuracy()
    {
        // Genel isabet oran�n� g�ncelle.
        accuracyPercentage = totalShots > 0 ? ((float)successfulHits / totalShots) * 100.0f : 100.0f;
        accuracyPercentage = Mathf.Clamp(accuracyPercentage, 0.0f, 100.0f); // Ba�ar�y� 0 ile 100 aras�nda tut

        hitPercentage.text = $"Accuracy: {accuracyPercentage:F2}%";
    }
}
