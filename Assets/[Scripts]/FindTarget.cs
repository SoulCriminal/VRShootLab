using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindTarget : MonoBehaviour
{
    TargetManager targetManager;
    ShootingStats shootingStats;
    private void Start()
    {
        targetManager = TargetManager.Instance;
        shootingStats = ShootingStats.Instance;
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Enemy"))
        { 
            if(targetManager.isCountdownActive)
            {
                targetManager.IncreaseScore();
                targetManager.SpawnRandomTarget();
                shootingStats.FireStats(true);
                Debug.Log("Deneme");
                Destroy(collision.gameObject);
            }
        }
        
    }
}
