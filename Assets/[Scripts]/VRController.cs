using InfimaGames.LowPolyShooterPack;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class VRController : MonoBehaviour
{
    // Silah nesnesi için referans
    public Weapon2 weapon;
    public AudioSource fireAudioSource; // Ses çalmak için AudioSource referansı
    [SerializeField] AudioSource emtyFireSource;
    public static bool isStart = false;

    private void Start()
    {
        XRGrabInteractable grabbable = GetComponent<XRGrabInteractable>();
        grabbable.activated.AddListener(FireWeapon);

        //Oyunun başlaması ve ateş eventi için 
       // isStart = true;
        // AudioSource bileşeni oluştur
        InitializeFireAudioSource();
    }

    void FireWeapon(ActivateEventArgs arg)
    {
        Debug.LogWarning("Ateş Edilmeye Çalışıldı");
        if (weapon != null && isStart)
        {
            weapon.Fire();
            Debug.LogError("Ateş Edildi");

            // Ateş edildiğinde ses çal
            PlayFireSound();

            // Total shots değerini ShootingStats sınıfına güncelle ve vuruş durumunu belirt
            ShootingStats.totalShots++;
            ShootingStats.Instance.FireStats(false);
        }
        else if (weapon != null && !isStart)
        {
            emtyFireSource.Play();
        }
    }

    /// <summary>
    /// Initialize the fire audio source
    /// </summary>
    private void InitializeFireAudioSource()
    {
        // Null check for fire audio source
        if (fireAudioSource == null)
        {
            // Create an empty game object for the audio source
            GameObject audioSourceObject = new GameObject("FireAudioSource");
            audioSourceObject.transform.parent = transform; // Attach it to the VR controller

            // Add an AudioSource component
            fireAudioSource = audioSourceObject.AddComponent<AudioSource>();
        }

        // Diğer ses ayarları burada yapılabilir
    }

    /// <summary>
    /// Play the fire sound using the fire audio source
    /// </summary>
    private void PlayFireSound()
    {
        // Check if the audio source is assigned
        if (fireAudioSource != null)
        {
            // Play the fire sound
            fireAudioSource.Play();
        }
    }
    void PlayEmptySoound()
    {
        if(emtyFireSource !=null)
        {
            emtyFireSource.Play();
        }
    }  
}